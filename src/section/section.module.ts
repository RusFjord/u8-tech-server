import { Module } from '@nestjs/common'
import { SectionService } from './section.service'
import { SectionResolver } from './section.resolver'
import { MongooseModule } from '@nestjs/mongoose'
import { Section, SectionSchema } from './models/section.entity'

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: Section.name,
                schema: SectionSchema,
            },
        ]),
    ],
    providers: [SectionService, SectionResolver],
})
export class SectionModule {}
