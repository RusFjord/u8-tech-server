import { Args, Mutation, Query, Resolver } from '@nestjs/graphql'
import { SectionService } from './section.service'
import { Section } from './models/section.entity'
import { SectionDto } from './models/section.dto'
import { SectionUpdateDto } from './models/section-update.dto'

@Resolver()
export class SectionResolver {
    constructor(private readonly sectionService: SectionService) {}
    @Query(() => [Section])
    getAllSection() {
        return this.sectionService.getAllSection()
    }

    @Query(() => Section)
    getSectionById(@Args('id') id: string) {
        return this.sectionService.getSectionById(id)
    }

    @Mutation(() => Section)
    createSection(@Args('payload') payload: SectionDto) {
        return this.sectionService.createSection(payload)
    }
    @Mutation(() => Section)
    updateSection(@Args('payload') payload: SectionUpdateDto) {
        return this.sectionService.updateSection(payload)
    }
}
