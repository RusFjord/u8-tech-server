import { Injectable } from '@nestjs/common'
import { Section } from './models/section.entity'
import { InjectModel } from '@nestjs/mongoose'
import { Model, Types } from 'mongoose'
import { SectionDto } from './models/section.dto'
import { SectionUpdateDto } from './models/section-update.dto'

@Injectable()
export class SectionService {
    constructor(
        @InjectModel(Section.name) private sectionModel: Model<Section>
    ) {}
    async getAllSection(): Promise<Section[]> {
        return this.sectionModel.find().exec()
    }
    async createSection(sectionDto: SectionDto): Promise<Section> {
        const section = new this.sectionModel({
            _id: new Types.ObjectId(),
            ...sectionDto,
        })
        return section.save()
    }

    async getSectionById(id: string): Promise<Section> {
        return this.sectionModel.findById(new Types.ObjectId(id))
    }

    async updateSection(sectionUpdateDto: SectionUpdateDto): Promise<Section> {
        const { id, ...update } = sectionUpdateDto
        return this.sectionModel
            .findOneAndUpdate(
                {
                    _id: new Types.ObjectId(id),
                },
                update
            )
            .exec()
    }
}
