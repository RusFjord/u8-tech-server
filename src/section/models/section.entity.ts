import { Field, ObjectType } from '@nestjs/graphql'
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { HydratedDocument } from 'mongoose'
import { CommonObject } from 'src/common-object/common-object.entity'

export type SectionDocument = HydratedDocument<Section>

@Schema()
@ObjectType({ implements: CommonObject })
export class Section extends CommonObject {
    @Prop({ required: true })
    @Field()
    title: string
    @Prop({ required: false })
    @Field({ nullable: true })
    image?: string
    @Prop({ required: false })
    @Field({ nullable: true })
    icon?: string
    @Prop({ required: false })
    @Field({ nullable: true })
    body?: string
}

export const SectionSchema = SchemaFactory.createForClass(Section)
