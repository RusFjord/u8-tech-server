import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class SectionDto {
    @Field()
    title: string
    @Field({ nullable: true })
    image?: string
    @Field({ nullable: true })
    icon?: string
    @Field({ nullable: true })
    body?: string
}
