import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class SectionUpdateDto {
    @Field({ nullable: false })
    id: string

    @Field({ nullable: true })
    title?: string
    @Field({ nullable: true })
    image?: string
    @Field({ nullable: true })
    icon?: string
    @Field({ nullable: true })
    body?: string
}
