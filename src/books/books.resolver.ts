import { Args, Query, Resolver } from '@nestjs/graphql'
import { BooksService } from './books.service'
import { Books } from './models/books.entity'

@Resolver()
export class BooksResolver {
    constructor(private readonly booksService: BooksService) {}
    @Query(() => [Books])
    getAllBooks() {
        return this.booksService.getAllBooks()
    }
    @Query(() => Books)
    getBookByID(@Args('id') id: string) {
        return this.getBookByID(id)
    }
}
