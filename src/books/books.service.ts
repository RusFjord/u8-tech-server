import { Injectable } from '@nestjs/common'
import { getBooks } from 'db/books.repositories'

@Injectable()
export class BooksService {
    async getAllBooks() {
        return getBooks()
    }

    async getBookByID(id: string) {
        return getBooks(id)
    }
}
