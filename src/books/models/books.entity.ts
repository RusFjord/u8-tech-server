import { Field, ObjectType } from '@nestjs/graphql'
import { Authors } from './authors.entity'
import { Series } from './series.entity'
import { Property } from 'src/common-object/common-object.entity'

@ObjectType()
export class Books {
    @Field()
    id: string
    @Field()
    title: string
    @Field()
    description: string
    @Field(() => [Authors])
    authors: Authors[]
    @Field(() => [Series])
    series?: Series[]
    @Field(() => [String])
    tags?: string[]
    @Field(() => [Property])
    properties?: Property[]
}
