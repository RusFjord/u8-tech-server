import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class Authors {
    @Field()
    id: string
    @Field()
    name: string
}
