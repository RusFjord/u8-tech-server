import { Module } from '@nestjs/common'
import { ArticleService } from './article.service'
import { MongooseModule } from '@nestjs/mongoose'
import { Article, ArticleSchema } from './models/article.entity'
import { ArticleResolver } from './article.resolver'
import { AuthModule } from 'src/auth/auth.module'

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Article.name, schema: ArticleSchema },
        ]),
        AuthModule,
    ],
    providers: [ArticleService, ArticleResolver],
})
export class ArticleModule {}
