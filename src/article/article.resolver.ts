import { Args, Mutation, Query, Resolver } from '@nestjs/graphql'
import { Article } from './models/article.entity'
import { ArticleService } from './article.service'
import { ArticleDto } from './models/article.dto'
import { UseGuards } from '@nestjs/common'
import { AuthGuard } from 'src/auth/auth.guard'
import { User as CurrentUser } from 'src/user/models/user.decorator'
import { User } from 'src/user/models/user.entity'

@Resolver()
export class ArticleResolver {
    constructor(private readonly articleService: ArticleService) {}

    @Query(() => [Article])
    @UseGuards(AuthGuard)
    getAllArticle(@CurrentUser() user: User) {
        return this.articleService.getAllArticles()
    }

    @Query(() => Article)
    getArticleById(@Args('id') id: string) {
        return this.articleService.getArticleByID(id)
    }

    @Mutation(() => Article)
    createArticle(@Args('payload') payload: ArticleDto) {
        return this.articleService.createArticle(payload)
    }
}
