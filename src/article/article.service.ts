import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Article } from './models/article.entity'
import { Model, Types } from 'mongoose'
import { ArticleDto } from './models/article.dto'
import { PublicationStatus } from 'src/common-object/common-object.entity'

@Injectable()
export class ArticleService {
    constructor(
        @InjectModel(Article.name) private articleModel: Model<Article>
    ) {}

    async getAllArticles(): Promise<Article[]> {
        return this.articleModel.find().populate(['section'])
    }

    async createArticle(articleDto: ArticleDto): Promise<Article> {
        const now = Date.now().toString()
        const { sectionId } = articleDto
        const article = new this.articleModel({
            _id: new Types.ObjectId(),
            created: now,
            changed: now,
            section: new Types.ObjectId(sectionId),
            status: PublicationStatus.DRAFT,
            ...articleDto,
        })
        return article.save()
    }

    async getArticleByID(id: string): Promise<Article> {
        return this.articleModel.findById(new Types.ObjectId(id))
    }
}
