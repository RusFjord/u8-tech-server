import { Field, ObjectType } from '@nestjs/graphql'
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { HydratedDocument, Types } from 'mongoose'
import {
    PublicationStatus,
    TimeCommonObject,
} from 'src/common-object/common-object.entity'
import { Section } from 'src/section/models/section.entity'

export type ArticleDocument = HydratedDocument<Article>

@Schema()
@ObjectType({ implements: () => TimeCommonObject })
export class Article extends TimeCommonObject {
    @Prop({ required: true })
    @Field()
    title: string
    @Prop({ required: false })
    @Field({ nullable: true })
    image?: string
    @Prop({ required: true })
    @Field()
    body: string
    @Prop({ required: false })
    @Field(() => [String], { nullable: true })
    tags?: string[]
    @Prop({ required: true })
    @Field(() => PublicationStatus, { defaultValue: PublicationStatus.DRAFT })
    status: PublicationStatus
    @Prop({ required: true, type: Types.ObjectId, ref: 'Section' })
    @Field(() => Section)
    section: Section
}

export const ArticleSchema = SchemaFactory.createForClass(Article)
