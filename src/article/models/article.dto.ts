import { Field, InputType } from '@nestjs/graphql'
import { PublicationStatus } from 'src/common-object/common-object.entity'

@InputType()
export class ArticleDto {
    @Field()
    title: string
    @Field()
    body: string
    @Field()
    sectionId: string
    // @Field()
    // status: PublicationStatus
}
