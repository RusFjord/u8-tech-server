import { Injectable } from '@nestjs/common'
import { Category } from './models/category.entity'
import { InjectModel } from '@nestjs/mongoose'
import { Model, Types } from 'mongoose'
import { CategoryDto } from './models/category.dto'
import { CategoryUpdateDto } from './models/category-update.dto'

@Injectable()
export class CategoryService {
    constructor(
        @InjectModel(Category.name)
        private categoryModel: Model<Category>
    ) {}

    async getAllCategories(): Promise<Category[]> {
        return this.categoryModel.find().populate(['parent']).exec()
    }

    async getCategoryById(id: string): Promise<Category> {
        return this.categoryModel
            .findById(new Types.ObjectId(id))
            .populate(['parent'])
    }

    async createCategory(categoryDto: CategoryDto): Promise<Category> {
        const { parentId } = categoryDto
        const category = new this.categoryModel({
            _id: new Types.ObjectId(),
            parent:
                parentId === undefined ? null : new Types.ObjectId(parentId),
            ...categoryDto,
        })
        return (await category.save()).populate(['parent'])
    }

    async updateCategory(
        categoryUpdateDto: CategoryUpdateDto
    ): Promise<Category> {
        const { id, parentId, ...update } = categoryUpdateDto
        const category = this.categoryModel.findOneAndUpdate(
            {
                _id: new Types.ObjectId(id),
            },
            {
                parent: new Types.ObjectId(parentId),
                ...update,
            }
        )
        return this.getCategoryById((await category)._id)
    }
}
