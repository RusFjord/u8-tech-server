import { Field, ObjectType } from '@nestjs/graphql'
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { HydratedDocument, Types } from 'mongoose'
import { CommonObject } from 'src/common-object/common-object.entity'

export type CategoryDocument = HydratedDocument<Category>

@Schema()
@ObjectType({ implements: () => CommonObject })
export class Category extends CommonObject {
    @Prop({ required: true })
    @Field()
    title: string
    @Prop({ required: false, type: Types.ObjectId, ref: 'Category' })
    @Field({ nullable: true })
    parent?: Category
    @Prop({ required: false })
    @Field({ nullable: true })
    image?: string
    @Prop({ required: false })
    @Field({ nullable: true })
    icon?: string
    @Prop({ required: false })
    @Field({ nullable: true })
    body?: string
}

export const CategoryModel = SchemaFactory.createForClass(Category)
CategoryModel.virtual('id').get(_id => _id)
