import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class CategoryDto {
    @Field()
    title: string
    @Field({ nullable: true })
    parentId?: string
    @Field({ nullable: true })
    image?: string
    @Field({ nullable: true })
    icon?: string
    @Field({ nullable: true })
    body?: string
}
