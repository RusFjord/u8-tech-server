import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class CategoryUpdateDto {
    @Field({ nullable: false })
    id: string
    @Field({ nullable: true })
    title?: string
    @Field({ nullable: true })
    parentId?: string
    @Field({ nullable: true })
    image?: string
    @Field({ nullable: true })
    icon?: string
    @Field({ nullable: true })
    body?: string
}
