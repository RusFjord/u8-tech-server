import { Args, Mutation, Query, Resolver } from '@nestjs/graphql'
import { Category } from './models/category.entity'
import { CategoryService } from './category.service'
import { CategoryDto } from './models/category.dto'
import { CategoryUpdateDto } from './models/category-update.dto'

@Resolver()
export class CategoryResolver {
    constructor(private readonly categoryService: CategoryService) {}
    @Query(() => [Category])
    getAllCategories() {
        return this.categoryService.getAllCategories()
    }
    @Query(() => Category)
    getCategoryById(@Args('id') id: string) {
        return this.categoryService.getCategoryById(id)
    }
    @Mutation(() => Category)
    createCategory(@Args('payload') payload: CategoryDto) {
        return this.categoryService.createCategory(payload)
    }
    @Mutation(() => Category)
    updateCategory(@Args('payload') payload: CategoryUpdateDto) {
        return this.categoryService.updateCategory(payload)
    }
}
