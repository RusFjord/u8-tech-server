import { Module } from '@nestjs/common'
import { CategoryService } from './category.service'
import { CategoryResolver } from './category.resolver'
import { MongooseModule } from '@nestjs/mongoose'
import { Category, CategoryModel } from './models/category.entity'

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Category.name, schema: CategoryModel },
        ]),
    ],
    providers: [CategoryService, CategoryResolver],
})
export class CategoryModule {}
