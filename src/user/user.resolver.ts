import { Args, Mutation, Resolver } from '@nestjs/graphql'
import { User } from './models/user.entity'
import { UserUpdate } from './models/user-update.input'
import { UserService } from './user.service'

@Resolver()
export class UserResolver {
    constructor(private userService: UserService) {}
    @Mutation(() => User)
    updateUser(@Args('userUpdate') userUpdate: UserUpdate): Promise<User> {
        return this.userService.updateUser(userUpdate)
    }
}
