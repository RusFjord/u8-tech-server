import { Module } from '@nestjs/common'
import { UserService } from './user.service'
import { MongooseModule } from '@nestjs/mongoose'
import { User, UserSchema } from './models/user.entity'
import { UserResolver } from './user.resolver';

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: User.name,
                schema: UserSchema,
            },
        ]),
    ],
    exports: [UserService],
    providers: [UserService, UserResolver],
})
export class UserModule {}
