import { Field, ObjectType } from '@nestjs/graphql'
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { HydratedDocument } from 'mongoose'
import { TimeCommonObject } from 'src/common-object/common-object.entity'

export type UserDocument = HydratedDocument<User>

@Schema()
@ObjectType({ implements: () => TimeCommonObject })
export class User extends TimeCommonObject {
    @Prop({ required: true, unique: true })
    @Field()
    username: string
    @Prop({ required: true })
    @Field()
    email: string
    @Prop({ required: false })
    @Field({ nullable: true })
    fullName?: string
    @Prop({ required: false })
    @Field({ nullable: true, defaultValue: false })
    isAdmin?: boolean
    @Prop({ required: false })
    @Field({ nullable: true })
    bio?: string
    @Prop({ required: false })
    @Field({ nullable: true })
    image?: string
    @Prop({ required: true })
    password: string
}

export const UserSchema = SchemaFactory.createForClass(User)
