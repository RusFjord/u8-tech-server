import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class UserUpdate {
    @Field()
    id: string
    @Field({ nullable: true })
    username?: string
    @Field({ nullable: true })
    email?: string
    @Field({ nullable: true })
    fullName?: string
    @Field({ nullable: true })
    isAdmin?: boolean
    @Field({ nullable: true })
    bio?: string
    @Field({ nullable: true })
    image?: string
}
