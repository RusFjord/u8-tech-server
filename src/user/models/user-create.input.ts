import { Field, InputType } from '@nestjs/graphql'
import { IUser } from 'src/common-object/common-object.entity'

@InputType()
export class UserCreate implements IUser {
    @Field()
    username: string
    @Field()
    email: string
    @Field()
    password: string
    @Field({ nullable: true })
    fullName?: string
    @Field({ nullable: true, defaultValue: false })
    isAdmin?: boolean
    @Field({ nullable: true })
    bio?: string
    @Field({ nullable: true })
    image?: string
}
