import { Injectable } from '@nestjs/common'
import { User } from './models/user.entity'
import { InjectModel } from '@nestjs/mongoose'
import { Model, Types } from 'mongoose'
import { IUser } from 'src/common-object/common-object.entity'
import { UserUpdate } from './models/user-update.input'
import { ForbiddenError } from '@nestjs/apollo'

@Injectable()
export class UserService {
    constructor(@InjectModel(User.name) private userModel: Model<User>) {}

    async getAllUsers(): Promise<User[]> {
        return this.userModel.find().exec()
    }

    async getUserById(id: string): Promise<User> {
        return this.userModel.findById(new Types.ObjectId(id))
    }

    async getUserByUsername(username: string): Promise<User> | undefined {
        return this.userModel.findOne({ username }).exec()
    }

    async createUser(userInput: IUser): Promise<User> {
        delete userInput['password']
        const user = new this.userModel({
            _id: new Types.ObjectId(),
            password: userInput.passwordHash,
            ...userInput,
        })
        return user.save()
    }

    async updateUser(userUpdate: UserUpdate): Promise<User> {
        if (userUpdate.username !== undefined) {
            const userTest = await this.getUserByUsername(userUpdate.username)
            if (userTest !== null)
                throw new ForbiddenError('Такое имя пользователя уже занято.')
        }
        const { id, ...userData } = userUpdate
        return this.userModel
            .findByIdAndUpdate(new Types.ObjectId(id), userData, { new: true })
            .exec()
    }

    async updateUserPassword(userInput: IUser) {
        delete userInput['password']
        return this.userModel.findOneAndUpdate(
            { username: userInput.username },
            { password: userInput.passwordHash },
            { new: true }
        )
    }
}
