import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo'
import { Module } from '@nestjs/common'
import { GraphQLModule } from '@nestjs/graphql'
import { join } from 'path'
import { BooksModule } from './books/books.module'
import { ConfigModule } from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose'
import { ArticleModule } from './article/article.module'
import { SectionModule } from './section/section.module'
import { CategoryModule } from './category/category.module'
import { UserModule } from './user/user.module'
import { AuthModule } from './auth/auth.module'

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        MongooseModule.forRoot('mongodb://localhost/u8tech'),
        GraphQLModule.forRoot<ApolloDriverConfig>({
            driver: ApolloDriver,
            context: ({ req }) => ({ req }),
            autoSchemaFile: join(process.cwd(), process.env.SCHEMA_PATH),
            playground: true,
        }),
        BooksModule,
        ArticleModule,
        SectionModule,
        CategoryModule,
        UserModule,
        AuthModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
