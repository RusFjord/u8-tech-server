import {
    Injectable,
    InternalServerErrorException,
    NotFoundException,
    UnauthorizedException,
} from '@nestjs/common'
import { LoginResponse } from './models/login-response.entity'
import { AuthData } from './models/auth-data.input'
import { JwtService } from '@nestjs/jwt'
import { UserService } from 'src/user/user.service'
import * as bcrypt from 'bcrypt'
import { User } from 'src/user/models/user.entity'
import { AuthRegister } from './models/auth-register.input'
import { IUser } from 'src/common-object/common-object.entity'
import { AuthChangePassword } from './models/auth-change-password.input'

@Injectable()
export class AuthService {
    constructor(
        private jwtService: JwtService,
        private userService: UserService
    ) {}

    async login(authData: AuthData): Promise<LoginResponse> {
        const user = await this.validate(authData)
        const payload = {
            username: user.username,
            userId: user._id,
        }
        const loginResponse = {
            accessToken: await this.jwtService.sign(payload, {
                secret: process.env.JWT_SECRET,
            }),
            user,
        } as LoginResponse
        return loginResponse
    }

    async validate(authData: AuthData): Promise<User> {
        const user = await this.userService.getUserByUsername(authData.username)
        if (user === null)
            throw new NotFoundException(
                'Пользователь с таким именем не найден.'
            )

        const passwordCorrect = await bcrypt.compare(
            authData.password,
            user.password
        )
        if (!passwordCorrect)
            throw new UnauthorizedException('Пароль неверный.')
        return user
    }

    async register(authRegister: AuthRegister): Promise<boolean> {
        const user = await this.userService.getUserByUsername(
            authRegister.username
        )
        if (user !== null)
            throw new UnauthorizedException(
                'Невозможно зарегистрироваться с таким именем пользователя.'
            )
        const rounds = 10
        const newUserData: IUser = {
            passwordHash: await bcrypt.hash(authRegister.password, rounds),
            ...authRegister,
        }
        const newUser = await this.userService.createUser(newUserData)
        if (newUser === null)
            throw new UnauthorizedException('Пользователь не создан.')
        return true
    }

    async changePassword(authChangePassword: AuthChangePassword) {
        const user = await this.validate(authChangePassword)
        const rounds = 10
        const newUserData: IUser = {
            passwordHash: await bcrypt.hash(
                authChangePassword.newPassword,
                rounds
            ),
            ...user,
        }
        const newUser = await this.userService.createUser(newUserData)
        if (newUser === null)
            throw new InternalServerErrorException('Ошибка смены пароля.')
        return true
    }

    async validateToken(token: string) {
        let payload
        try {
            payload = this.jwtService.verify(token, {
                secret: process.env.JWT_SECRET,
            })
        } catch {
            throw new UnauthorizedException('Ошибка проверки токена.')
        }
        return payload
    }

    async findUserById(id: string): Promise<User> {
        return await this.userService.getUserById(id)
    }
}
