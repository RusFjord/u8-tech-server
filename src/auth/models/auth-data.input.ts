import { Field, InputType } from '@nestjs/graphql'
import { IUser } from 'src/common-object/common-object.entity'

@InputType()
export class AuthData implements IUser {
    @Field()
    username: string
    @Field()
    password: string
}
