import { Field, InputType } from '@nestjs/graphql'
import { IUser } from 'src/common-object/common-object.entity'

@InputType()
export class AuthRegister implements IUser {
    @Field()
    username: string
    @Field()
    email: string
    @Field()
    password: string
}
