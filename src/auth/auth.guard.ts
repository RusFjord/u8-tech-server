import {
    CanActivate,
    ExecutionContext,
    Injectable,
    UnauthorizedException,
} from '@nestjs/common'
import { GqlExecutionContext } from '@nestjs/graphql'
import { AuthService } from './auth.service'

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const ctx = GqlExecutionContext.create(context)
        const request = this.getRequest(ctx)
        const headers = request.headers
        const token = this.getToken(headers.authorization)

        const payload = await this.authService.validateToken(token)

        request.user = await this.authService.findUserById(payload.userId)
        ctx.getContext().user = request.user
        return true
    }

    getRequest(context: GqlExecutionContext) {
        return context.getContext().req
    }

    private getToken(authHeader: string): string {
        if (authHeader.indexOf('Bearer') !== 0) {
            throw new UnauthorizedException('Неверный метод авторизации')
        }
        return authHeader.replace('Bearer ', '')
    }
}
