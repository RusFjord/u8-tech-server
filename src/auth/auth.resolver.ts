import { Args, Mutation, Resolver } from '@nestjs/graphql'
import { LoginResponse } from './models/login-response.entity'
import { AuthData } from './models/auth-data.input'
import { AuthService } from './auth.service'
import { AuthRegister } from './models/auth-register.input'

@Resolver()
export class AuthResolver {
    constructor(private authService: AuthService) {}

    @Mutation(() => LoginResponse)
    login(@Args('authData') authData: AuthData): Promise<LoginResponse> {
        return this.authService.login(authData)
    }

    @Mutation(() => Boolean)
    register(@Args('authRegister') authRegister: AuthRegister) {
        return this.authService.register(authRegister)
    }
}
