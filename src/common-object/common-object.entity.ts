import {
    Field,
    ID,
    InterfaceType,
    ObjectType,
    registerEnumType,
} from '@nestjs/graphql'
import { Prop } from '@nestjs/mongoose'

@InterfaceType()
export abstract class CommonObject {
    @Prop()
    @Field(() => ID)
    _id: string
}

@InterfaceType()
export abstract class TimeCommonObject extends CommonObject {
    @Prop()
    @Field(() => String)
    created: string
    @Prop()
    @Field(() => String)
    changed: string
}

@ObjectType()
export class Property {
    @Field()
    name: string
    @Field()
    value: string
}

export enum PublicationStatus {
    DRAFT,
    MODERATION,
    PUBLISHED,
    PERSONAL,
    FOR_ADMIN,
}

registerEnumType(PublicationStatus, { name: 'PublicationStatus' })

export interface IUser {
    username: string
    password: string
    email?: string
    newPassword?: string
    passwordHash?: string
}
